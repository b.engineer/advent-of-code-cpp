#include <algorithm>
#include <bitset>
#include <climits>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <math.h>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_4.h"
#include "file_utils.h"

using namespace std;

AocDay4::AocDay4() : AocDay(4) {}

AocDay4::~AocDay4() {}

// vector<long> AocDay4::read_input(string filename) {
//   FileUtils fileutils;
//   vector<string> raw_lines;
//   vector<long> data;
//   if (!fileutils.read_as_list_of_strings(filename, raw_lines)) {
//     cerr << "Error reading in the data from " << filename << endl;
//     return data;
//   }
//   for (vector<string>::iterator iter = raw_lines.begin();
//        iter != raw_lines.end(); ++iter) {
//     long l;
//     string to_convert = *iter;
//     l = strtol(to_convert.c_str(), NULL, 10);
//     data.push_back(l);
//   }
//   return data;
// }

vector<string> AocDay4::read_input(string filename) {
  FileUtils fileutils;
  vector<string> raw_lines;
  if (!fileutils.read_as_list_of_strings(filename, raw_lines)) {
    cerr << "Error reading in the data from " << filename << endl;
  }
  return raw_lines;
}

string AocDay4::part1(string filename, vector<string> extra_args) {
  // 3d vector source https://iq.opengenus.org/3d-vectors-in-cpp/
  for (string arg : extra_args) {
    cout << arg << endl;
  }

  vector<string> data = read_input(filename);
  long result = 0;
  // boards / row / column / number
  int x = 100, y = 5, z = 5;
  vector<vector<vector<int>>> boards(
      x, vector<vector<int>>(y, vector<int>(z, -1)));
  vector<int> winningboards;

  // parse stack
  // https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
  size_t pos = 0;
  string delimiter = ",";
  vector<int> stack;

  while ((pos = data[0].find(delimiter)) != string::npos) {
    stack.push_back(stoi(data[0].substr(0, pos)));
    data[0].erase(0, pos + delimiter.length());
  }
  stack.push_back(stoi(data[0]));

  // parse boards
  int board = 0;
  int row = 0;
  for (vector<string>::iterator iter_line = data.begin();
       iter_line != data.end(); ++iter_line) {
    if ((*iter_line).size() == 14) {
      if (row < y) {
        for (int col = 0; col < z; col++) {
          boards[board][row][col] = stoi((*iter_line).substr(col * 3, 2));
        }
        row++;
      } else {
        row = 0;
      }
    } else if (row == y) {
      board++;
      row = 0;
    }
  }

  // loop: draw number from stack, update boards, check boards for win
  for (auto draw = stack.begin(); draw != stack.end(); ++draw) {

    for (int i = 0; i < x; i++) {
      for (int j = 0; j < y; j++) {
        for (int k = 0; k < z; k++) {
          if (boards[i][j][k] == *draw) {
            boards[i][j][k] = -1;

            // check board for win
            bool bingo = false;
            // check rows
            for (int j = 0; j < y; j++) {
              bool bingo_row = true;
              for (int k = 0; k < z; k++) {
                if (boards[i][j][k] != -1) {
                  bingo_row = false;
                }
              }
              if (bingo_row) {
                if (find(winningboards.begin(), winningboards.end(), i) ==
                    winningboards.end()) {
                  winningboards.push_back(i);
                  bingo = true;
                }
              }
            }
            // check columns
            for (int k = 0; k < z; k++) {
              bool bingo_col = true;
              for (int j = 0; j < y; j++) {
                if (boards[i][j][k] != -1) {
                  bingo_col = false;
                }
              }
              if (bingo_col) {
                if (find(winningboards.begin(), winningboards.end(), i) ==
                    winningboards.end()) {
                  winningboards.push_back(i);
                  bingo = true;
                }
              }
            }

            // calc solution for part1
            if (bingo == true && winningboards.size() == 1 &&
                extra_args[0] == "") {
              int boardsum = 0;
              for (int j = 0; j < y; j++) {
                for (int k = 0; k < z; k++) {
                  if (boards[i][j][k] != -1) {
                    boardsum += boards[i][j][k];
                  }
                }
              }
              result = boardsum * (*draw);
            }

            // calc solution for part2
            if (bingo == true && winningboards.size() == 100 &&
                extra_args[0] == "part2") {
              int boardsum = 0;
              for (int j = 0; j < y; j++) {
                for (int k = 0; k < z; k++) {
                  if (boards[i][j][k] != -1) {
                    boardsum += boards[i][j][k];
                  }
                }
              }
              result = boardsum * (*draw);
            }
          } // if number = draw
        }   // for loop col
      }     // for loop row
    }       // for loop board

    // print boards
    // for (int i = 0; i < x; i++) {
    //   for (int j = 0; j < y; j++) {
    //     for (int k = 0; k < z; k++) {
    //       cout << boards[i][j][k] << ", ";
    //     }
    //     cout << endl;
    //   }
    // }
  }

  // for (int i = 0; i < winningboards.size(); i++) {
  //   cout << winningboards[i] << ", ";
  // }

  ostringstream out;
  out << result;
  return out.str();
}

string AocDay4::part2(string filename, vector<string> extra_args) {
  vector<string> args = {"part2"};
  return AocDay4::part1(filename, args);
}
