#include <climits>
#include <cstdlib>
#include <iostream>
#include <limits.h>
#include <queue>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_15.h"
#include "file_utils.h"

using namespace std;

AocDay15::AocDay15() : AocDay(0) {}

AocDay15::~AocDay15() {}

// Define the size of the cave
const int N = 10;

// Define the directions: right and down
const vector<pair<int, int>> directions = {{1, 0}, {0, 1}};

// Function to check if a position is valid
bool isValid(int x, int y) { return x >= 0 && x < N && y >= 0 && y < N; }

// Dijkstra's algorithm to find the lowest total risk
int dijkstra(const vector<vector<int>> &cave) {
  // Create a 2D vector to store the minimum total risk for each position
  vector<vector<int>> minRisk(N, vector<int>(N, INT_MAX));

  // Create a priority queue to store the nodes with the lowest total risk
  priority_queue<pair<int, pair<int, int>>, vector<pair<int, pair<int, int>>>,
                 greater<pair<int, pair<int, int>>>>
      pq;

  // Initialize the starting position with risk level 0
  minRisk[0][0] = 0;
  pq.push({0, {0, 0}});

  while (!pq.empty()) {
    int currRisk = pq.top().first;
    int x = pq.top().second.first;
    int y = pq.top().second.second;
    pq.pop();

    // Check if we have reached the destination
    if (x == N - 1 && y == N - 1) {
      for (const auto &innerVec : minRisk) {
        for (const auto &num : innerVec) {
          std::cout << num << ' ';
        }
        std::cout << '\n';
      }
      return currRisk;
    }

    // Explore the neighboring positions
    for (const auto &dir : directions) {
      int newX = x + dir.first;
      int newY = y + dir.second;

      // Check if the new position is valid
      if (isValid(newX, newY)) {
        int newRisk = currRisk + cave[newY][newX];

        // Update the minimum total risk if necessary
        if (newRisk < minRisk[newY][newX]) {
          minRisk[newY][newX] = newRisk;
          pq.push({newRisk, {newX, newY}});
        }
      }
    }
  }

  // If no path is found, return -1
  return -1;
}

string AocDay15::part1(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  vector<vector<int>> cave;
  for (const string &row : lines) {
    vector<int> grid_row;
    for (int risk : row) {
      grid_row.push_back(risk - 48);
    }
    cave.push_back(grid_row);
  }

  // Find the lowest total risk using Dijkstra's algorithm
  int lowestRisk = dijkstra(cave);

  // Print the result
  cout << "Lowest total risk: " << lowestRisk << endl;

  out << "nothing";
  return out.str();
}

string AocDay15::part2(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  out << "nothing";
  return out.str();
}
