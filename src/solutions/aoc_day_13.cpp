#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

#include "aoc_day_13.h"
#include "file_utils.h"

using namespace std;

AocDay13::AocDay13() : AocDay(0) {}

AocDay13::~AocDay13() {}

struct pair_hash {
  template <class T1, class T2>
  std::size_t operator()(std::pair<T1, T2> const &p) const {
    std::size_t h1 = std::hash<T1>()(p.first);
    std::size_t h2 = std::hash<T2>()(p.second);

    // Mainly for demonstration purposes, i.e. works but is overly simple
    // In the real world, use sth. like boost.hash_combine
    return h1 ^ h2;
  }
};

std::pair<std::vector<std::pair<int, int>>, std::vector<std::pair<char, int>>>
parseInput(string filename) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;

  std::vector<std::pair<int, int>> dots;
  std::vector<std::pair<char, int>> foldInstructions;

  for (string line : lines) {
    if (line.empty()) {
      continue;
    }
    if (line.substr(0, 11) == "fold along ") {
      char axis = line[11];
      int position = std::stoi(line.substr(13));
      foldInstructions.push_back({axis, position});
    } else {
      int x = std::stoi(line.substr(0, line.find(',')));
      int y = std::stoi(line.substr(line.find(',') + 1));
      dots.push_back({x, y});
    }
  }

  return {dots, foldInstructions};
}

std::unordered_set<std::pair<int, int>, pair_hash>
foldPaper(std::vector<std::pair<int, int>> &dots,
          std::vector<std::pair<char, int>> &foldInstructions, int numFolds) {
  std::unordered_set<std::pair<int, int>, pair_hash> paper;

  // Add the dots to the set
  for (const auto &dot : dots) {
    paper.insert(dot);
  }

  for (int i = 0; i < numFolds; i++) {
    const auto &foldInstruction = foldInstructions[i];
    if (foldInstruction.first == 'x') {
      // Fold along x-axis
      for (auto it = paper.begin(); it != paper.end();) {
        if (it->first > foldInstruction.second) {
          int newX = 2 * foldInstruction.second - it->first;
          paper.insert({newX, it->second});
          it = paper.erase(it);
        } else {
          ++it;
        }
      }
    } else {
      // Fold along y-axis
      for (auto it = paper.begin(); it != paper.end();) {
        if (it->second > foldInstruction.second) {
          int newY = 2 * foldInstruction.second - it->second;
          paper.insert({it->first, newY});
          it = paper.erase(it);
        } else {
          ++it;
        }
      }
    }
  }

  return paper;
}

void displayDots(std::unordered_set<std::pair<int, int>, pair_hash> &paper) {
  int maxX = 0;
  int maxY = 0;
  for (const auto &dot : paper) {
    maxX = std::max(maxX, dot.first);
    maxY = std::max(maxY, dot.second);
  }

  for (int y = 0; y <= maxY; y++) {
    for (int x = 0; x <= maxX; x++) {
      auto it = paper.find({x, y});
      if (it != paper.end()) {
        std::cout << '#';
      } else {
        std::cout << '.';
      }
    }
    std::cout << std::endl;
  }
}

string AocDay13::part1(string filename, vector<string> extra_args) {
  auto [dots, foldInstructions] = parseInput(filename);
  auto paper = foldPaper(dots, foldInstructions, 1);

  displayDots(paper);

  int visibleDots = paper.size();
  ostringstream out;
  out << "visible dots:" << visibleDots;
  return out.str();
}

string AocDay13::part2(string filename, vector<string> extra_args) {
  auto [dots, foldInstructions] = parseInput(filename);
  auto paper = foldPaper(dots, foldInstructions, foldInstructions.size());

  displayDots(paper);

  int visibleDots = paper.size();
  ostringstream out;

  out << "LGHEGUEJ";
  return out.str();
}
