#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stack>
#include <string>
#include <vector>

#include "aoc_day_9.h"
#include "file_utils.h"

using namespace std;

AocDay9::AocDay9() : AocDay(9) {}

AocDay9::~AocDay9() {}

string AocDay9::part1(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  vector<vector<int>> heights;
  for (const string &row : lines) {
    vector<int> heights_row;
    for (int height : row) {
      heights_row.push_back(height - 48);
    }
    heights.push_back(heights_row);
  }

  int risk_level_sum = 0;
  // Check each location for being a low point
  for (int i = 0; i < heights.size(); i++) {
    for (int j = 0; j < heights[0].size(); j++) {
      int cur_height = heights[i][j];
      bool is_low_point = true;
      // Check all adjacent locations
      for (int di = -1; di <= 1; di++) {
        for (int dj = -1; dj <= 1; dj++) {
          if (di * dj == 0 && di + dj != 0) {
            // Check if the adjacent location is lower
            int i2 = i + di;
            int j2 = j + dj;
            if (i2 >= 0 && i2 < heights.size() && j2 >= 0 &&
                j2 < heights[0].size() && heights[i2][j2] <= cur_height) {
              is_low_point = false;
            }
          }
        }
      }
      // If the location is a low point, add its risk level to the sum
      if (is_low_point) {
        risk_level_sum += cur_height + 1;
        // cout << "." << heights[i][j];
      } else {
        // cout << " " << heights[i][j];
      }
    }
    cout << endl;
  }

  out << risk_level_sum;
  return out.str();
}

string AocDay9::part2(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  vector<vector<int>> heights;
  for (const string &row : lines) {
    vector<int> heights_row;
    for (int height : row) {
      heights_row.push_back(height - 48);
    }
    heights.push_back(heights_row);
  }

  vector<pair<int, int>> low_points;

  // Open a files for writing
  std::ofstream outlowpoints("data/day9_lowpoints.txt");
  std::ofstream outsearchpoints("data/day9_searchpoints.txt");
    
  // Check if the files was opened successfully
  if (!outlowpoints.is_open() or !outsearchpoints.is_open()) {
      std::cerr << "Failed to open file for writing." << std::endl;
  }
  

  // Check each location for being a low point
  for (int i = 0; i < heights.size(); i++) {
    for (int j = 0; j < heights[0].size(); j++) {
      int cur_height = heights[i][j];
      bool is_low_point = true;
      // Check all adjacent locations
      for (int di = -1; di <= 1; di++) {
        for (int dj = -1; dj <= 1; dj++) {
          if (di * dj == 0 && di + dj != 0) {
            // Check if the adjacent location is lower
            int i2 = i + di;
            int j2 = j + dj;
            if (i2 >= 0 && i2 < heights.size() && j2 >= 0 &&
                j2 < heights[0].size() && heights[i2][j2] <= cur_height) {
              is_low_point = false;
            }
          }
        }
      }
      // If the location is a low point, add it to the vector low_points
      if (is_low_point) {
        low_points.push_back(make_pair(i, j));
        outlowpoints << i << "," << j << endl;
      }
    }
  }

  // Initialize a vector to keep track of which locations have been visited
  vector<vector<bool>> visited(heights.size(),
                               vector<bool>(heights[0].size(), false));

  // Initialize a vector to keep track of which basin each location belongs to
  vector<vector<int>> basins(heights.size(), vector<int>(heights[0].size(), 0));

  // Perform a depth-first search starting from each low point
  for (int i = 0; i < low_points.size(); i++) {
    int row = low_points[i].first;
    int col = low_points[i].second;
    if (!visited[row][col]) {
      // Initialize the basin ID to the index of the low point
      int basin_id = i + 1;
      stack<pair<int, int>> stack;
      stack.push({row, col});
      while (!stack.empty()) {
        
        int r = stack.top().first;
        int c = stack.top().second;
        stack.pop();
        if (!visited[r][c]) {
          // save order of search points to file
          outsearchpoints << r << "," << c << endl;

          visited[r][c] = true;
          basins[r][c] = basin_id;
          // Add unvisited neighbors to the stack
          if (r > 0 && heights[r - 1][c] < 9)
            stack.push({r - 1, c});
          if (r < heights.size() - 1 && heights[r + 1][c] < 9)
            stack.push({r + 1, c});
          if (c > 0 && heights[r][c - 1] < 9)
            stack.push({r, c - 1});
          if (c < heights[0].size() - 1 && heights[r][c + 1] < 9)
            stack.push({r, c + 1});
        }
      }
    }
  }


  // Count the number of locations in each basin to find the basin sizes
  vector<int> basin_sizes(low_points.size(), 0);
  for (int r = 0; r < basins.size(); r++) {
    for (int c = 0; c < basins[0].size(); c++) {
      if (basins[r][c] > 0) {
        basin_sizes[basins[r][c] - 1]++;
      }
    }
  }

  // Sort the basin sizes in descending order and multiply together the sizes of
  // the three largest basins
  sort(basin_sizes.rbegin(), basin_sizes.rend());
  int answer = 1;
  for (int i = 0; i < 3 && i < basin_sizes.size(); i++) {
    answer *= basin_sizes[i];
  }

  
  // Close the file
  outlowpoints.close();
  outsearchpoints.close();

  out << answer;
  return out.str();
}
