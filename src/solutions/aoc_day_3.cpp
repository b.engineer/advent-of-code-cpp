#include <bitset>
#include <climits>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <math.h>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_3.h"
#include "file_utils.h"

using namespace std;

AocDay3::AocDay3() : AocDay(3) {}

AocDay3::~AocDay3() {}

// vector<long> AocDay3::read_input(string filename) {
//   FileUtils fileutils;
//   vector<string> raw_lines;
//   vector<long> data;
//   if (!fileutils.read_as_list_of_strings(filename, raw_lines)) {
//     cerr << "Error reading in the data from " << filename << endl;
//     return data;
//   }
//   for (vector<string>::iterator iter = raw_lines.begin();
//        iter != raw_lines.end(); ++iter) {
//     long l;
//     string to_convert = *iter;
//     l = strtol(to_convert.c_str(), NULL, 10);
//     data.push_back(l);
//   }
//   return data;
// }

vector<string> AocDay3::read_input(string filename) {
  FileUtils fileutils;
  vector<string> raw_lines;
  if (!fileutils.read_as_list_of_strings(filename, raw_lines)) {
    cerr << "Error reading in the data from " << filename << endl;
  }
  return raw_lines;
}

vector<string> AocDay3::calc_most_common_bits(vector<string> data) {
  vector<long> number_of_0bits(12, 0);
  vector<long> number_of_1bits(12, 0);
  int gamma_rate = 0;
  int epsilon_rate = 0;
  vector<string> result(2, "");

  for (vector<string>::iterator iter_line = data.begin();
       iter_line != data.end(); ++iter_line) {
    for (string::iterator iter_bit = iter_line->begin();
         iter_bit != iter_line->end(); ++iter_bit) {
      int index = distance(iter_line->begin(), iter_bit);
      if (*iter_bit == '0') {
        number_of_0bits[index]++;
      } else if (*iter_bit == '1') {
        number_of_1bits[index]++;
      } else {
        cout << "Error: unknown input character";
      }
    }
  }

  for (int i = 0; i != 12; i++) {
    if (number_of_0bits[i] <= number_of_1bits[i]) {
      gamma_rate += pow(2, 11 - i);
    } else if (number_of_0bits[i] > number_of_1bits[i]) {
      epsilon_rate += pow(2, 11 - i);
    } else {
      cout << "Warning: number of zeros and ones are equal for index: " << i
           << "\n";
    }
  }
  result[0] = std::bitset<12>(gamma_rate).to_string();
  result[1] = std::bitset<12>(epsilon_rate).to_string();
  return result;
}

string AocDay3::part1(string filename, vector<string> extra_args) {
  vector<string> data = read_input(filename);
  long result = 0;

  vector<long> number_of_0bits(12, 0);
  vector<long> number_of_1bits(12, 0);
  int gamma_rate = 0;
  int epsilon_rate = 0;

  for (vector<string>::iterator iter_line = data.begin();
       iter_line != data.end(); ++iter_line) {

    for (string::iterator iter_bit = iter_line->begin();
         iter_bit != iter_line->end(); ++iter_bit) {
      int index = distance(iter_line->begin(), iter_bit);
      if (*iter_bit == '0') {
        number_of_0bits[index]++;
      } else if (*iter_bit == '1') {
        number_of_1bits[index]++;
      } else {
        cout << "Error: unknown input character";
      }
    }
  }

  for (int i = 0; i != 12; i++) {
    if (number_of_0bits[i] < number_of_1bits[i]) {
      gamma_rate += pow(2, 11 - i);
    } else if (number_of_0bits[i] > number_of_1bits[i]) {
      epsilon_rate += pow(2, 11 - i);
    } else {
      cout << "Warning: number of zeros and ones are equal for index: " << i
           << "\n";
    }
  }
  result = gamma_rate * epsilon_rate;

  ostringstream out;
  out << result;
  return out.str();
}

string AocDay3::part2(string filename, vector<string> extra_args) {
  vector<string> data_oxygen = read_input(filename);
  vector<string> data_co2 = read_input(filename);
  long result = 0;
  vector<string> most_common_bits;
  string gamma_rate;
  string epsilon_rate;
  int oxygen_rating = 0;
  int co2_rating = 0;

  int bitposition = 0; // index
  while (data_oxygen.size() > 1) {
    most_common_bits = calc_most_common_bits(data_oxygen);
    gamma_rate = most_common_bits[0];
    epsilon_rate = most_common_bits[1];
    for (vector<string>::iterator iter_line = data_oxygen.begin();
         iter_line != data_oxygen.end();) {
      string current_data_point = *iter_line;
      if (current_data_point[bitposition] != gamma_rate[bitposition]) {
        iter_line = data_oxygen.erase(iter_line);
      } else {
        ++iter_line;
      }
    }
    bitposition++;
  }
  oxygen_rating = stoi(data_oxygen[0], nullptr, 2);

  bitposition = 0; // index
  while (data_co2.size() > 1) {
    most_common_bits = calc_most_common_bits(data_co2);
    gamma_rate = most_common_bits[0];
    epsilon_rate = most_common_bits[1];
    for (vector<string>::iterator iter_line = data_co2.begin();
         iter_line != data_co2.end();) {
      string current_data_point = *iter_line;
      if (current_data_point[bitposition] != epsilon_rate[bitposition]) {
        iter_line = data_co2.erase(iter_line);
      } else {
        ++iter_line;
      }
    }
    bitposition++;
  }
  co2_rating = stoi(data_co2[0], nullptr, 2);

  result = oxygen_rating * co2_rating;
  ostringstream out;
  out << result;
  return out.str();
}
