#include <algorithm>
#include <iostream>
#include <map>
#include <sstream>
#include <stack>
#include <string>
#include <vector>

#include "aoc_day_10.h"
#include "file_utils.h"

using namespace std;

AocDay10::AocDay10() : AocDay(10) {}

AocDay10::~AocDay10() {}

// Helper function to check if two characters form a legal pair
bool is_legal_pair(char open, char close) {
  return (open == '(' && close == ')') || (open == '[' && close == ']') ||
         (open == '{' && close == '}') || (open == '<' && close == '>');
}

// Helper function to calculate the syntax error score
int calculate_syntax_error_score(char illegal_char) {
  static const map<char, int> syntax_error_scores{
      {')', 3}, {']', 57}, {'}', 1197}, {'>', 25137}};
  return syntax_error_scores.at(illegal_char);
}

// Function to find the first illegal character in a corrupted chunk
char find_illegal_char(const string &chunk) {
  stack<char> opening_chars;
  for (char c : chunk) {
    if (c == '(' || c == '[' || c == '{' || c == '<') {
      opening_chars.push(c);
    } else if (c == ')' || c == ']' || c == '}' || c == '>') {
      if (opening_chars.empty() || !is_legal_pair(opening_chars.top(), c)) {
        return c;
      }
      opening_chars.pop();
    }
  }
  return ' '; // No illegal character found
}

string AocDay10::part1(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  int total_syntax_error_score = 0;

  for (const string &line : lines) {
    bool is_corrupted = false;
    for (size_t i = 0; i < line.size(); i++) {
      char c = line[i];
      if (c == ')' || c == ']' || c == '}' || c == '>') {
        char illegal_char = find_illegal_char(line.substr(0, i + 1));
        if (illegal_char != ' ') {
          int syntax_error_score = calculate_syntax_error_score(illegal_char);
          total_syntax_error_score += syntax_error_score;
          is_corrupted = true;
          break;
        }
      }
    }
    if (is_corrupted) {
      // out << "Corrupted line: " << line << endl;
    } else {
      // out << "Valid line: " << line << endl;
    }
  }

  out << total_syntax_error_score << endl;
  return out.str();
}

// Given an incomplete line, return the completion string that closes all open
// chunks
string completeLine(string line) {
  stack<char> openChunks;

  for (char c : line) {
    switch (c) {
    case '(':
      openChunks.push(c);
      break;
    case '[':
      openChunks.push(c);
      break;
    case '{':
      openChunks.push(c);
      break;
    case '<':
      openChunks.push(c);
      break;
    case ')':
      if (openChunks.empty() || openChunks.top() != '(') {
        return "";
      }
      openChunks.pop();
      break;
    case ']':
      if (openChunks.empty() || openChunks.top() != '[') {
        return "";
      }
      openChunks.pop();
      break;
    case '}':
      if (openChunks.empty() || openChunks.top() != '{') {
        return "";
      }
      openChunks.pop();
      break;
    case '>':
      if (openChunks.empty() || openChunks.top() != '<') {
        return "";
      }
      openChunks.pop();
      break;
    }
  }

  string completion = "";
  while (!openChunks.empty()) {
    switch (openChunks.top()) {
    case '(':
      completion += ')';
      break;
    case '[':
      completion += ']';
      break;
    case '{':
      completion += '}';
      break;
    case '<':
      completion += '>';
      break;
    }
    openChunks.pop();
  }

  // reverse(completion.begin(), completion.end());
  return completion;
}

// Given a completion string, calculate its score according to the rules
long scoreCompletion(string completion) {
  long score = 0;
  for (char c : completion) {
    score = score * 5;
    switch (c) {
    case ')':
      score += 1;
      break;
    case ']':
      score += 2;
      break;
    case '}':
      score += 3;
      break;
    case '>':
      score += 4;
      break;
    }
  }
  return score;
}

string AocDay10::part2(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  vector<long> scores;
  for (string line : lines) {
    string completion = completeLine(line);
    if (completion != "") {
      scores.push_back(scoreCompletion(completion));
    }
  }

  sort(scores.begin(), scores.end());
  long middleScore = scores[scores.size() / 2];


  out << "Middle score: " << middleScore << endl;
  return out.str();
}
