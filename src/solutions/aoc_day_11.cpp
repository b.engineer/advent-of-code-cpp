#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_11.h"
#include "file_utils.h"

using namespace std;

AocDay11::AocDay11() : AocDay(11) {}

AocDay11::~AocDay11() {}

void updateAdjacentOct(int i, int j, vector<vector<int>> &updatedOctopuses,
                       vector<vector<bool>> &octopusesFlashed) {
  int n = updatedOctopuses.size();
  int m = updatedOctopuses[0].size();

  // std::cout << i << "" << j << "";

  if (updatedOctopuses[i][j] == 0)
    return; // Stop recursion if the octopus has already flashed

  if (updatedOctopuses[i][j] > 9) {
    updatedOctopuses[i][j] = 0; // Flash the current octopus
    octopusesFlashed[i][j] = true;

    // increase adjacent octopuses
    if (i > 0) {
      if (updatedOctopuses[i - 1][j] <= 9)
        updatedOctopuses[i - 1][j]++;
      if (j > 0 && updatedOctopuses[i - 1][j - 1] <= 9)
        updatedOctopuses[i - 1][j - 1]++;
      if (j < m - 1 && updatedOctopuses[i - 1][j + 1] <= 9)
        updatedOctopuses[i - 1][j + 1]++;
    }
    if (i < n - 1) {
      if (updatedOctopuses[i + 1][j] <= 9)
        updatedOctopuses[i + 1][j]++;
      if (j > 0 && updatedOctopuses[i + 1][j - 1] <= 9)
        updatedOctopuses[i + 1][j - 1]++;
      if (j < m - 1 && updatedOctopuses[i + 1][j + 1] <= 9)
        updatedOctopuses[i + 1][j + 1]++;
    }
    if (j > 0 && updatedOctopuses[i][j - 1] <= 9)
      updatedOctopuses[i][j - 1]++;
    if (j < m - 1 && updatedOctopuses[i][j + 1] <= 9)
      updatedOctopuses[i][j + 1]++;

    // Recursively update the adjacent octopuses
    if (i > 0) {
      if (j > 0)
        updateAdjacentOct(i - 1, j - 1, updatedOctopuses, octopusesFlashed);
      if (j < m - 1)
        updateAdjacentOct(i - 1, j + 1, updatedOctopuses, octopusesFlashed);
      updateAdjacentOct(i - 1, j, updatedOctopuses, octopusesFlashed);
    }
    if (i < n - 1) {
      if (j > 0)
        updateAdjacentOct(i + 1, j - 1, updatedOctopuses, octopusesFlashed);
      if (j < m - 1)
        updateAdjacentOct(i + 1, j + 1, updatedOctopuses, octopusesFlashed);
      updateAdjacentOct(i + 1, j, updatedOctopuses, octopusesFlashed);
    }
    if (j > 0)
      updateAdjacentOct(i, j - 1, updatedOctopuses, octopusesFlashed);
    if (j < m - 1)
      updateAdjacentOct(i, j + 1, updatedOctopuses, octopusesFlashed);
  }
}

string AocDay11::part1(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  vector<vector<int>> octopuses, updatedOctopuses;
  for (const string &row : lines) {
    vector<int> octopuses_row;
    for (int octopus : row) {
      octopuses_row.push_back(octopus - 48);
    }
    octopuses.push_back(octopuses_row);
  }

  int totalFlashes = 0;
  int n = octopuses.size();
  int m = octopuses[0].size();
  vector<vector<bool>> octopusesFlashed;

  // Simulate 100 steps
  for (int step = 1; step <= 100; step++) {
    octopusesFlashed = vector(n, vector(m, false));

    // Increase energy level of each octopus by 1
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        octopuses[i][j]++;
      }
    }

    updatedOctopuses = octopuses; // current state of octopuses

    // Update adjacent octopuses and count flashes
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        if (octopuses[i][j] > 9) {
          octopusesFlashed[i][j] = true;
          updateAdjacentOct(i, j, updatedOctopuses, octopusesFlashed);
        }
      }
    }

    // Reset flashed octopuses and count total flashes
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        if (octopusesFlashed[i][j] == true) {
          totalFlashes++;
          updatedOctopuses[i][j] = 0;
        }
      }
    }

    octopuses = updatedOctopuses; // Update octopuses with the new energy levels

    // debug
    // for (const auto &innerVec : octopuses) {
    //   for (const auto &num : innerVec) {
    //     std::cout << num << "";
    //   }
    //   std::cout << std::endl;
    // }
    // std::cout << std::endl;
  }

  out << totalFlashes;
  return out.str();
}

string AocDay11::part2(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  vector<vector<int>> octopuses, updatedOctopuses;
  for (const string &row : lines) {
    vector<int> octopuses_row;
    for (int octopus : row) {
      octopuses_row.push_back(octopus - 48);
    }
    octopuses.push_back(octopuses_row);
  }

  int totalFlashes = 0;
  int n = octopuses.size();
  int m = octopuses[0].size();
  vector<vector<bool>> octopusesFlashed;
  int firstSyncFlash = 0;

  // Simulate 100 steps
  for (int step = 1; step <= 250; step++) {
    octopusesFlashed = vector(n, vector(m, false));

    // Increase energy level of each octopus by 1
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        octopuses[i][j]++;
      }
    }

    updatedOctopuses = octopuses; // current state of octopuses

    // Update adjacent octopuses and count flashes
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        if (octopuses[i][j] > 9) {
          octopusesFlashed[i][j] = true;
          updateAdjacentOct(i, j, updatedOctopuses, octopusesFlashed);
        }
      }
    }

    // Reset flashed octopuses and count total flashes
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        if (octopusesFlashed[i][j] == true) {
          totalFlashes++;
          updatedOctopuses[i][j] = 0;
        }
      }
    }

    octopuses = updatedOctopuses; // Update octopuses with the new energy levels

    if (octopusesFlashed == vector(n, vector(m, true)) and
        firstSyncFlash == 0) {
      firstSyncFlash = step;
    }
  }

  out << firstSyncFlash;
  return out.str();
}
