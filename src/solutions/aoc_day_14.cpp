#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_14.h"
#include "file_utils.h"

using namespace std;

AocDay14::AocDay14() : AocDay(0) {}

AocDay14::~AocDay14() {}
///////////////////////////////// PART1
std::map<std::string, char> parse_rules(const std::vector<std::string> &lines) {
  std::map<std::string, char> rule_map;
  for (std::size_t i = 1; i < lines.size(); i++) {
    const auto &line = lines[i];
    if (line.empty()) {
      continue; // Skip empty lines
    }
    char insert = line[line.size() - 1];
    rule_map[line.substr(0, 2)] = insert;
  }
  return rule_map;
}

std::map<char, long long> count_elements(const std::string &polymer) {
  std::map<char, long long> counts;
  for (char c : polymer) {
    counts[c]++;
  }
  return counts;
};

std::string apply_rules(const std::string &polymer,
                        const std::map<std::string, char> &rules, int steps) {
  std::string new_polymer = polymer;
  for (int i = 0; i < steps; i++) {
    std::string next_polymer;
    next_polymer.reserve(new_polymer.size() + steps);
    for (std::string::size_type j = 0; j < new_polymer.size(); j++) {
      next_polymer += new_polymer[j];
      if (j < new_polymer.size() - 1) {
        std::string pair = new_polymer.substr(j, 2);
        if (rules.find(pair) != rules.end()) {
          next_polymer += rules.at(pair);
        }
      }
    }
    new_polymer = next_polymer;
  }
  return new_polymer;
};

string AocDay14::part1(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  std::string polymer = lines[0];
  std::map<std::string, char> rule_map = parse_rules(lines);

  std::string new_polymer = apply_rules(polymer, rule_map, 10);

  std::map<char, long long> counts = count_elements(new_polymer);

  char max_char = (*std::max_element(counts.begin(), counts.end(),
                                     [](const auto &a, const auto &b) {
                                       return a.second < b.second;
                                     }))
                      .first;

  char min_char = (*std::min_element(counts.begin(), counts.end(),
                                     [](const auto &a, const auto &b) {
                                       return a.second < b.second;
                                     }))
                      .first;
  long long diff = counts[max_char] - counts[min_char];
  out << diff;

  // for (auto count : counts) {
  //   out << std::endl << "count: " << count.first << " " << count.second;
  // }

  return out.str();
}

////////////////////////////// PART2
std::map<std::string, long long> count_pairs(const std::string &polymer) {
  std::map<std::string, long long> counts;
  for (std::string::size_type i = 0; i < polymer.size() - 1; i++) {
    std::string pair = polymer.substr(i, 2);
    counts[pair]++;
  }
  return counts;
}

std::map<std::string, long long>
apply_rules_pairs(const std::map<std::string, long long> &pairs,
                const std::map<std::string, char> &rules, int steps) {
 std::map<std::string, long long> new_pairs = pairs;
 for (int i = 0; i < steps; i++) {
   std::map<std::string, long long> next_pairs;
   for (const auto &pair : new_pairs) {
     std::string new_pair = std::string(1, pair.first[0]) + rules.at(pair.first);
     next_pairs[new_pair] += pair.second;
     new_pair = std::string(1, rules.at(pair.first)) + pair.first[1];
     next_pairs[new_pair] += pair.second;
   }
   new_pairs = next_pairs;
 }
 return new_pairs;
}

std::map<char, long long>
count_elements_pairs(const std::map<std::string, long long> &pairs) {
  std::map<char, long long> counts;
  for (const auto &pair : pairs) {
    counts[pair.first[0]] += pair.second;
  }
  return counts;
}

string AocDay14::part2(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  std::string polymer = lines[0];
  std::map<std::string, char> rule_map = parse_rules(lines);
  std::map<std::string, long long> pairs = count_pairs(polymer);
  std::map<std::string, long long> new_pairs =
      apply_rules_pairs(pairs, rule_map, 40);

  
  std::map<char, long long> counts = count_elements_pairs(new_pairs);
  counts[polymer.back()]++;

  for (const auto &pair : counts) {
    std::cout << "Key: " << pair.first << ", Value: " << pair.second
              << std::endl;
  }

  char max_char = (*std::max_element(counts.begin(), counts.end(),
                                     [](const auto &a, const auto &b) {
                                       return a.second < b.second;
                                     }))
                      .first;
  char min_char = (*std::min_element(counts.begin(), counts.end(),
                                     [](const auto &a, const auto &b) {
                                       return a.second < b.second;
                                     }))
                      .first;

  long long diff = counts[max_char] - counts[min_char];
  out << diff;

  return out.str();
}
