#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_7.h"
#include "file_utils.h"

using namespace std;

AocDay7::AocDay7() : AocDay(0) {}

AocDay7::~AocDay7() {}

string AocDay7::part1(string filename, vector<string> extra_args) {
  vector<vector<long>> split_longs = {};
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_split_longs(filename, split_longs, ',', '"',
                                             '#'))
    cerr << "Error reading in the data from " << filename << endl;
  auto crabs = split_longs[0];
  int maxCrab = *max_element(crabs.begin(), crabs.end());
  vector<int> fuelList(maxCrab);
  ostringstream out;
  out << endl;

  for (int position = 0; position <= fuelList.size(); position++) {
    int fuel = 0;
    for (long crab : crabs) {
      fuel = fuel + abs(crab - position);
      fuelList[position] = fuel;
    }
  }

  out << *min_element(fuelList.begin(), fuelList.end()) << endl;
  return out.str();
}

string AocDay7::part2(string filename, vector<string> extra_args) {
  vector<vector<long>> split_longs = {};
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_split_longs(filename, split_longs, ',', '"',
                                             '#'))
    cerr << "Error reading in the data from " << filename << endl;
  auto crabs = split_longs[0];
  int maxCrab = *max_element(crabs.begin(), crabs.end());
  vector<int> fuelList(maxCrab);
  ostringstream out;
  out << endl;

  for (int position = 0; position <= fuelList.size(); position++) {
    int fuel = 0;
    for (long crab : crabs) {
      int fuelCrab = 0;
      for (int i = 0; i <= abs(crab - position); i++) {
        fuelCrab += i;
      }
      fuel += fuelCrab;
    }
    fuelList[position] = fuel;
  }

  out << *min_element(fuelList.begin(), fuelList.end()) << endl;
  return out.str();
}
