#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_16.h"
#include "file_utils.h"

using namespace std;

AocDay16::AocDay16() : AocDay(0) {}

AocDay16::~AocDay16() {}

string AocDay16::part1(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  out << "nothing";
  return out.str();
}

string AocDay16::part2(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  out << "nothing";
  return out.str();
}
