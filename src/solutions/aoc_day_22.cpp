#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_22.h"
#include "file_utils.h"

using namespace std;

AocDay22::AocDay22() : AocDay(0) {}

AocDay22::~AocDay22() {}

string AocDay22::part1(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  out << "nothing";
  return out.str();
}

string AocDay22::part2(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  out << "nothing";
  return out.str();
}
