#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_8.h"
#include "file_utils.h"

using namespace std;

AocDay8::AocDay8() : AocDay(0) {}

AocDay8::~AocDay8() {}

string AocDay8::part1(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;

  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;
  out << endl;

  // 1: 2, 4: 4, 7: 3, 8: 7
  int count = 0;
  for (string line : lines) {
    string lineOutput = line.substr(line.find("|"), line.size());
    int pos1 = 0;
    int pos2 = 0;
    for (int i = 0; i <= 3; i++) {
      pos1 = lineOutput.find(" ", pos1 + 1);
      pos2 = lineOutput.find(" ", pos1 + 1);
      int wordLength =
          (pos2 == -1) ? lineOutput.size() - pos1 - 1 : pos2 - pos1 - 1;
      // out << lineOutput << ", " << pos1 << ", " << pos2 << ", " << wordLength
      // << ", \n";
      if (wordLength == 2 || wordLength == 4 || wordLength == 3 ||
          wordLength == 7)
        count++;
    }
  }
  out << count << "\n";

  // out << lines[0].substr(0, lines[0].find("|")) << endl;
  // out << lines[0].substr(lines[0].find("|"), lines[0].size());
  return out.str();
}

std::string findChars(const std::string &str1, const std::string &str2,
                      bool findUniqueChars) {
  std::set<char> commonChars;
  std::set<char> seenChars;
  std::string result;

  if (findUniqueChars) {
    for (char c : str1) {
      if (seenChars.find(c) == seenChars.end()) {
        seenChars.insert(c);
      }
    }

    for (char c : str2) {
      if (seenChars.find(c) == seenChars.end()) {
        seenChars.insert(c);
      } else {
        seenChars.erase(c);
      }
    }

    for (char c : seenChars) {
      result += c;
    }
  } else {
    for (char c : str1) {
      seenChars.insert(c);
    }

    for (char c : str2) {
      if (seenChars.find(c) != seenChars.end()) {
        commonChars.insert(c);
      }
    }

    for (char c : commonChars) {
      result += c;
    }
  }

  return result;
}

bool compareStringLength(const std::string &str1, const std::string &str2) {
  return str1.length() < str2.length();
}

// length 2: 1
// length 3: 7
// length 4: 4
// length 5: 2, 3, 5
// length 6: 0, 6, 9
// length 7: 8

string AocDay8::part2(string filename, vector<string> extra_args) {
  vector<string> lines;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;
  out << endl;
  int result = 0;

  for (string line : lines) {
    string lineInput = line.substr(0, line.find("|"));
    string lineOutput = line.substr(line.find("|") + 2, line.size());
    int lineResult = 0;

    // read in data
    std::string delimiter = " ";
    size_t pos = 0;
    std::string token;
    std::vector<std::string> tokenVecInput;
    std::vector<std::string> lookupVec(10, "");
    std::vector<std::string> tokenVecOutput;

    while ((pos = lineInput.find(delimiter)) != std::string::npos) {
      token = lineInput.substr(0, pos);
      std::sort(token.begin(), token.end());
      tokenVecInput.push_back(token);
      lineInput.erase(0, pos + delimiter.length());
    }

    while ((pos = lineOutput.find(delimiter)) != std::string::npos) {
      token = lineOutput.substr(0, pos);
      std::sort(token.begin(), token.end());
      tokenVecOutput.push_back(token);
      lineOutput.erase(0, pos + delimiter.length());
    }
    if (!lineOutput.empty()) {
      std::sort(lineOutput.begin(), lineOutput.end());
      tokenVecOutput.push_back(lineOutput);
    }

    // create lookup vector
    std::sort(tokenVecInput.begin(), tokenVecInput.end(), compareStringLength);
    lookupVec[1] = tokenVecInput[0];
    lookupVec[7] = tokenVecInput[1];
    lookupVec[4] = tokenVecInput[2];
    lookupVec[8] = tokenVecInput[9];
    for (int i = 3; i <= 8; ++i) {
      if (i <= 5) {
        if (findChars(tokenVecInput[i], lookupVec[1], false).length() == 2)
          lookupVec[3] = tokenVecInput[i];
        else if (findChars(tokenVecInput[i], lookupVec[4], false).length() ==
                 2) {
          lookupVec[2] = tokenVecInput[i];
        } else {
          lookupVec[5] = tokenVecInput[i];
        }
      } else {
        if (findChars(tokenVecInput[i], lookupVec[4], false).length() == 4)
          lookupVec[9] = tokenVecInput[i];
        else if (findChars(tokenVecInput[i], lookupVec[1], false).length() == 2)
          lookupVec[0] = tokenVecInput[i];
        else {
          lookupVec[6] = tokenVecInput[i];
        }
      }
    }

    // decode lineOutput
    for (int i = 0; i <= 3; ++i) {
      for (int j = 0; j < lookupVec.size(); j++) {
        if (tokenVecOutput[i].compare(lookupVec[j]) == 0) {
          lineResult += j * std::pow(10, 3 - i);
        }
      }
    }

    // std::cout << lineResult << std::endl;
    result += lineResult;
  }

  out << result;
  return out.str();
}
