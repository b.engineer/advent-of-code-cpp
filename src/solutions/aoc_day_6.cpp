#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_6.h"
#include "file_utils.h"

using namespace std;

AocDay6::AocDay6() : AocDay(0) {}

AocDay6::~AocDay6() {}

vector<long> AocDay6::read_input(string filename) {
  FileUtils fileutils;
  vector<string> raw_lines;
  vector<long> data;
  if (!fileutils.read_as_list_of_strings(filename, raw_lines)) {
    cerr << "Error reading in the data from " << filename << endl;
    return data;
  }
  for (vector<string>::iterator iter = raw_lines.begin();
       iter != raw_lines.end(); ++iter) {
    long l;
    string to_convert = *iter;
    l = strtol(to_convert.c_str(), NULL, 10);
    data.push_back(l);
  }
  return data;
}

string AocDay6::part1(string filename, vector<string> extra_args) {
  int days = 80;
  vector<vector<long>> split_longs = {};
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_split_longs(filename, split_longs, ',', '"',
                                             '#'))
    cerr << "Error reading in the data from " << filename << endl;
  auto flock = split_longs[0];
  int currentFlockSize;
  ostringstream out;
  out << endl;

  for (int day = 0; day != days + 1; day++) {
    currentFlockSize = flock.size();
    // out << "day " << day << ", size: " << currentFlockSize << ": ";
    for (int fishIndex = 0; fishIndex < currentFlockSize; ++fishIndex) {
      // out << flock[fishIndex] << ",";
      if (flock[fishIndex] == 0) {
        flock[fishIndex] = 6;
        flock.push_back(8);
      } else {
        flock[fishIndex]--;
      }
    }
    // out << endl;
  }

  out << "size:" << currentFlockSize;
  return out.str();
}

string AocDay6::part2(string filename, vector<string> extra_args) {
  int days = 256;
  vector<vector<long>> split_longs = {};
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_split_longs(filename, split_longs, ',', '"',
                                             '#'))
    cerr << "Error reading in the data from " << filename << endl;
  auto flockUnsorted = split_longs[0];
  std::vector<long> flock(9, 0);
  long currentFlockSize = 0;
  ostringstream out;
  out << endl;

  // sort flock into flock
  for (long fish : flockUnsorted) {
    flock[fish]++;
  }

  // create time loop, let loop iterate for 256 days
  // flock[0] needs to be 0 at the end of loop
  for (int day = 0; day < days; day++) {

    long fishAtTimer0 = flock[0];

    for (int i = 1; i <= 8; i++) {
      flock[i - 1] = flock[i];
    }
    
    flock[6] = flock[6] + fishAtTimer0;
    flock[8] = fishAtTimer0;
    
    out << "day " << day << ": ";
    for (long numberFishAtTimer : flock) {
       out << numberFishAtTimer << ", "; 
    }
    out << endl;
  }

  // sum over index of flock
  out << endl << "flock: " << endl;
  for (long numberFishAtAgeIndex : flock) {
    out << numberFishAtAgeIndex << endl;
    currentFlockSize = currentFlockSize + numberFishAtAgeIndex;
  }

  out << "size:" << currentFlockSize;
  out << endl;

  return out.str();
}
