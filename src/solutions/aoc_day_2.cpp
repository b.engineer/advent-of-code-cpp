#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_2.h"
#include "file_utils.h"

using namespace std;

AocDay2::AocDay2() : AocDay(2) {}

AocDay2::~AocDay2() {}

vector<vector<string>> AocDay2::read_input(string filename) {
  FileUtils fileutils;
  vector<string> raw_lines;
  vector<vector<string>> split_strings;
  if (!fileutils.read_as_list_of_split_strings(filename, split_strings, ' ',
                                               '\'', '#')) {
    cerr << "Error reading in the data from " << filename << endl;
  }
  return split_strings;
}

string AocDay2::part1(string filename, vector<string> extra_args) {
  auto data = read_input(filename);
  long horizontal_position = 0;
  long depth = 0;
  for (vector<vector<string>>::iterator iter = data.begin(); iter != data.end();
       ++iter) {

    string direction = iter->front();
    string value_string = iter->back();
    long value_long;
    value_long = strtol(value_string.c_str(), NULL, 10);

    if (direction == "forward") {
      horizontal_position += value_long;
    } else if (direction == "up") {
      depth -= value_long;
    } else if (direction == "down") {
      depth += value_long;
    } else {
      cout << "Error: unknown command";
    }

    // cout << iter->front() << ", " << iter->back() << ", " <<
    // horizontal_position
    // << ", " << depth << "\n";
  }
  ostringstream out;
  out << horizontal_position * depth;
  return out.str();
}

string AocDay2::part2(string filename, vector<string> extra_args) {
  auto data = read_input(filename);
  long horizontal_position = 0;
  long depth = 0;
  long aim = 0;
  for (vector<vector<string>>::iterator iter = data.begin(); iter != data.end();
       ++iter) {

    string direction = iter->front();
    string value_string = iter->back();
    long value_long;
    value_long = strtol(value_string.c_str(), NULL, 10);

    if (direction == "forward") {
      horizontal_position += value_long;
      depth += aim * value_long;
    } else if (direction == "up") {
      aim -= value_long;
    } else if (direction == "down") {
      aim += value_long;
    } else {
      cout << "Error: unknown command";
    }

    // cout << iter->front() << ", " << iter->back()
    // << ", position: " << horizontal_position << ", depth: " << depth
    // << ", aim: " << aim << "\n";
  }
  ostringstream out;
  out << horizontal_position * depth;
  return out.str();
}
