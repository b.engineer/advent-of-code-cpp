#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_1.h"
#include "file_utils.h"

using namespace std;

AocDay1::AocDay1() : AocDay(1) {}

AocDay1::~AocDay1() {}

vector<long> AocDay1::read_input(string filename) {
  FileUtils fileutils;
  vector<string> raw_lines;
  vector<long> data;
  if (!fileutils.read_as_list_of_strings(filename, raw_lines)) {
    cerr << "Error reading in the data from " << filename << endl;
    return data;
  }
  for (vector<string>::iterator iter = raw_lines.begin();
       iter != raw_lines.end(); ++iter) {
    long l;
    string to_convert = *iter;
    l = strtol(to_convert.c_str(), NULL, 10);
    data.push_back(l);
  }
  return data;
}

string AocDay1::part1(string filename, vector<string> extra_args) {
  vector<long> data = read_input(filename);
  long sum = 0;
  for (vector<long>::iterator iter = next(data.begin()); iter != data.end();
       ++iter) {
    if (*prev(iter) < *iter) {
      ++sum;
    };
    // cout << *prev(iter, 1) << ", " << *iter << ", " << incr << '\n';
  }
  ostringstream out;
  out << sum;
  return out.str();
}

string AocDay1::part2(string filename, vector<string> extra_args) {
  if (extra_args.size() > 0) {
    cout << "There are " << extra_args.size()
         << " extra arguments given:" << endl;
    for (vector<string>::iterator iter = extra_args.begin();
         iter != extra_args.end(); ++iter) {
      cout << "[" << *iter << "]" << endl;
    }
  }

  vector<long> data = read_input(filename);
  long sum = 0;
  for (vector<long>::iterator iter = next(data.begin(), 3); iter != data.end();
       ++iter) {
    long sum_window_0 = *prev(iter, 3) + *prev(iter, 2) + *prev(iter, 1);
    long sum_window_1 = *prev(iter, 2) + *prev(iter, 1) + *iter;
    if (sum_window_0 < sum_window_1) {
      ++sum;
    }
    // cout << *prev(iter, 2) << ", " << *prev(iter, 1) << ", " << *iter
    // << ", sum0:" << sum_window_0 << '\n';
    // cout << *prev(iter, 3) << ", " << *prev(iter, 2) << ", " << *prev(iter,
    // 1)
    // << ", sum1:" << sum_window_1 << '\n';
    // bool incr = sum_window_0 < sum_window_1;
    // cout << '\n' << incr << ", " << sum << '\n';
  }
  ostringstream out;
  out << sum;
  return out.str();
}
