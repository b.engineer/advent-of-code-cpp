#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "aoc_day_5.h"
#include "file_utils.h"

using namespace std;

AocDay5::AocDay5() : AocDay(0) {}

AocDay5::~AocDay5() {}

vector<string> AocDay5::read_input(string filename) {
  FileUtils fileutils;
  vector<string> raw_lines;
  if (!fileutils.read_as_list_of_strings(filename, raw_lines)) {
    cerr << "Error reading in the data from " << filename << endl;
  }
  return raw_lines;
}

vector<string> bisectstr(string inputstr, string delimiter) {
  vector<string> out;
  out.push_back(inputstr.substr(0, inputstr.find(delimiter)));
  out.push_back(inputstr.substr(inputstr.find(delimiter) + delimiter.length(),
                                inputstr.length()));
  return out;
}

string AocDay5::part1(string filename, vector<string> extra_args) {
  vector<string> data = read_input(filename);
  vector<string> points;
  int result = 0;

  for (vector<string>::iterator line = data.begin(); line != data.end();
       ++line) {
    vector<int> current_line; // x1, y1, x2, y2

    // std::cout << endl << *line;
    for (string section : bisectstr(*line, " -> ")) {
      for (string num : bisectstr(section, ",")) {
        current_line.push_back(stoi(num));
        // cout << stoi(num) << ",";
      }
    }
    // only consider vertical (x1 = x2)
    if (current_line[0] == current_line[2]) {
      // std::cout << ": vertical";
      // cout << endl;
      if (current_line[1] < current_line[3]) {
        for (int i = current_line[1]; i <= current_line[3]; i++) {
          // std::cout << endl << current_line[0] << "," << i;
          points.push_back(to_string(current_line[0]) + "," + to_string(i));
        }
      } else if (current_line[1] > current_line[3]) {
        for (int i = current_line[1]; i >= current_line[3]; i--) {
          // std::cout << endl << current_line[0] << "," << i;
          points.push_back(to_string(current_line[0]) + "," + to_string(i));
        }
      }

      // and horizontal lines (y1 = y2)
    } else if (current_line[1] == current_line[3]) {
      // std::cout << ": horizontal";
      // cout << endl;
      if (current_line[0] < current_line[2]) {
        for (int i = current_line[0]; i <= current_line[2]; i++) {
          // std::cout << endl << i << "," << current_line[1];
          points.push_back(to_string(i) + "," + to_string(current_line[1]));
        }
      } else if (current_line[0] > current_line[2]) {
        for (int i = current_line[0]; i >= current_line[2]; i--) {
          // std::cout << endl << i << "," << current_line[1];
          points.push_back(to_string(i) + "," + to_string(current_line[1]));
        }
      }

    } else {
      // cout << endl;
    }
    // std::cout << endl << bisectstr(bisectstr(*line, " -> ")[1], ",")[1];
  }

  // https://thispointer.com/c-how-to-find-duplicates-in-a-vector/
  // Create a map to store the frequency of each element in vector
  std::map<std::string, int> countMap;
  // Iterate over the vector and store the frequency of each element in map
  for (auto &elem : points) {
    auto result = countMap.insert(std::pair<std::string, int>(elem, 1));
    if (result.second == false)
      result.first->second++;
  }
  // Iterate over the map
  for (auto &elem : countMap) {
    // If frequency count is greater than 1 then its a duplicate element
    if (elem.second > 1) {
      // std::cout << elem.first << " :: " << elem.second << std::endl;
      result++;
    }

    // std::cout << elem.first << " :: " << elem.second << std::endl;
  }

  // return out.str();
  return to_string(result);
}

string AocDay5::part2(string filename, vector<string> extra_args) {
  vector<string> data = read_input(filename);
  vector<string> points;
  int result = 0;

  for (vector<string>::iterator line = data.begin(); line != data.end();
       ++line) {
    vector<int> current_line; // x1, y1, x2, y2

    // std::cout << endl << *line;
    for (string section : bisectstr(*line, " -> ")) {
      for (string num : bisectstr(section, ",")) {
        current_line.push_back(stoi(num));
        // cout << stoi(num) << ",";
      }
    }
    // only consider vertical (x1 = x2)
    if (current_line[0] == current_line[2]) {
      // std::cout << ": vertical";
      // cout << endl;
      if (current_line[1] < current_line[3]) {
        for (int i = current_line[1]; i <= current_line[3]; i++) {
          // std::cout << endl << current_line[0] << "," << i;
          points.push_back(to_string(current_line[0]) + "," + to_string(i));
        }
      } else if (current_line[1] > current_line[3]) {
        for (int i = current_line[1]; i >= current_line[3]; i--) {
          // std::cout << endl << current_line[0] << "," << i;
          points.push_back(to_string(current_line[0]) + "," + to_string(i));
        }
      }

      // and horizontal lines (y1 = y2)
    } else if (current_line[1] == current_line[3]) {
      // std::cout << ": horizontal";
      // cout << endl;
      if (current_line[0] < current_line[2]) {
        for (int i = current_line[0]; i <= current_line[2]; i++) {
          // std::cout << endl << i << "," << current_line[1];
          points.push_back(to_string(i) + "," + to_string(current_line[1]));
        }
      } else if (current_line[0] > current_line[2]) {
        for (int i = current_line[0]; i >= current_line[2]; i--) {
          // std::cout << endl << i << "," << current_line[1];
          points.push_back(to_string(i) + "," + to_string(current_line[1]));
        }
      }

      // diagonal lines (|x1 - x2| = |y1 - y2|)
    } else if (std::abs(current_line[0] - current_line[2]) ==
               std::abs(current_line[1] - current_line[3])) {
      // std::cout << *line << ": diagonal " << ", ";
      int diagonalDistance = abs(current_line[0] - current_line[2]);
      for (int i = 0; i <= diagonalDistance; i++){
        int x = current_line[0];
        int y = current_line[1];
        (x < current_line[2]) ? x = x+i : x = x-i;
        (y < current_line[3]) ? y = y+i : y = y-i;
        // std::cout << x << ", " << y << endl;
        points.push_back(to_string(x) + "," + to_string(y));
      }
      
    } else {
      // std::cout << endl << *line << endl;
    }
    // std::cout << endl << bisectstr(bisectstr(*line, " -> ")[1], ",")[1];
  }

  // https://thispointer.com/c-how-to-find-duplicates-in-a-vector/
  // Create a map to store the frequency of each element in vector
  std::map<std::string, int> countMap;
  // Iterate over the vector and store the frequency of each element in map
  for (auto &elem : points) {
    auto result = countMap.insert(std::pair<std::string, int>(elem, 1));
    if (result.second == false)
      result.first->second++;
  }
  // Iterate over the map
  for (auto &elem : countMap) {
    // If frequency count is greater than 1 then its a duplicate element
    if (elem.second > 1) {
      // std::cout << elem.first << " :: " << elem.second << std::endl;
      result++;
    }

    // std::cout << elem.first << " :: " << elem.second << std::endl;
  }

  // return out.str();
  return to_string(result);
}
