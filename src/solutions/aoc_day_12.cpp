#include <cctype>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "aoc_day_12.h"
#include "file_utils.h"

using namespace std;

AocDay12::AocDay12() : AocDay(0) {}

AocDay12::~AocDay12() {}

struct Path {
  vector<string> caves;
  unordered_map<string, int> caveVisits;

  // Function to check if a small cave is already visited in the path
  bool containsCave(const string &Cave) const {
    return caveVisits.count(Cave) > 0;
  }
  bool doubleVisitFlag = false;
};

// Modify the findAllPaths function to allow for the new rules
void findAllPaths(const unordered_map<string, vector<string>> &caves,
                  const string &currentCave, Path &currentPath,
                  int &pathCount) {
  // If the current cave is the start and it is already in the path, skip this
  // iteration
  if (currentCave == "start" && currentPath.containsCave(currentCave)) {
    return;
  }
  // Add the current cave to the path
  currentPath.caves.push_back(currentCave);

  // If the current cave is the end, increment the pathCount
  if (currentCave == "end") {
    for (const string &cave : currentPath.caves) {
      cout << cave;
      if (cave != "end") {
        cout << ",";
      }
    }
    cout << endl;

    // currentPath.doubleVisitFlag = false;
    pathCount++;
  } else {
    // Recursively explore all connected caves
    for (const string &nextCave : caves.at(currentCave)) {

      if (isupper(nextCave[0]) ||
          currentPath.caveVisits[nextCave] <=
              1) // big caves can be visited multiple times
      {
        // skip recursion of a small cave have been visited twice already and
        // next cave is also a visited small cave
        if (currentPath.caveVisits[nextCave] == 1 && islower(nextCave[0])) {
          if (currentPath.doubleVisitFlag) {
            continue;
          } else {
            currentPath.doubleVisitFlag = true;
          }
        }

        // If the next cave is a small cave, increment its visit count
        currentPath.caveVisits[nextCave]++;
        // Recursively explore the next cave
        findAllPaths(caves, nextCave, currentPath, pathCount);
        // If the next cave is a small cave, decrement its visit count when
        // backtracking
        currentPath.caveVisits[nextCave]--;
        if (currentPath.caveVisits[nextCave] == 1 && islower(nextCave[0])) {
          currentPath.doubleVisitFlag = false;
        };
      }
    }
  }

  // Remove the current cave from the path when backtracking
  currentPath.caves.pop_back();
}

string AocDay12::part1(string filename, vector<string> extra_args) {
  vector<string> lines;
  std::unordered_map<std::string, std::vector<std::string>> caves;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  for (const string &line : lines) {
    std::string cave0 = line.substr(0, line.find("-"));
    std::string cave1 = line.substr(line.find("-") + 1);

    caves[cave0].push_back(cave1);
    caves[cave1].push_back(cave0);
  }

  // Initialize variables
  int pathCount = 0;
  Path currentPath;
  unordered_set<string> visitedSmallCaves;

  // Find all paths from start to end
  // findAllPaths(caves, "start", currentPath, visitedSmallCaves, pathCount);

  // Output the total number of paths
  out << "Total number of paths: " << pathCount << endl;
  return out.str();
}

string AocDay12::part2(string filename, vector<string> extra_args) {
  vector<string> lines;
  std::unordered_map<std::string, std::vector<std::string>> caves;
  FileUtils fileutils;
  if (!fileutils.read_as_list_of_strings(filename, lines))
    cerr << "Error reading in the data from " << filename << endl;
  ostringstream out;

  for (const string &line : lines) {
    std::string cave0 = line.substr(0, line.find("-"));
    std::string cave1 = line.substr(line.find("-") + 1);

    caves[cave0].push_back(cave1);
    caves[cave1].push_back(cave0);
  }

  // Initialize variables
  int pathCount = 0;
  Path currentPath;

  // Find all paths from start to end
  findAllPaths(caves, "start", currentPath, pathCount);

  // Output the total number of paths
  out << "Total number of paths: " << pathCount << endl;
  return out.str();
}
