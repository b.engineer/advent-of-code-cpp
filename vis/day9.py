import bpy

# Get all objects in the scene
scene_objects = bpy.context.scene.objects

# Loop through each object
for obj in scene_objects:
    # Check if the object's name starts with "Cube"
    if obj.name.startswith("cube"):
        # Delete the object
        bpy.data.objects.remove(obj, do_unlink=True)

# check if "cubes" collection exists and delete it if it does
if "cubes" in bpy.data.collections:
    bpy.data.collections.remove(bpy.data.collections["cubes"], do_unlink=True)

# Define the file path of the text file
day9_data = "/home/bn/.local/src/advent-of-code-cpp/data/day9_test.txt"
day9_lowpoints = "/home/bn/.local/src/advent-of-code-cpp/data/day9_lowpoints.txt"
day9_searchpoints = "/home/bn/.local/src/advent-of-code-cpp/data/day9_searchpoints.txt"

# Read in the text file
with open(day9_data, "r") as f:
    lines = f.readlines()

# Get the number of rows from the number of lines in the text file
num_rows = len(lines)

# Get the number of columns from the number of digits in the first line of the text file
num_cols = len(lines[0].strip())

# Create a new collection
my_collection = bpy.data.collections.new("cubes")
bpy.context.scene.collection.children.link(my_collection)


# Create the grid of cubes
for row in range(num_rows):
    for col in range(num_cols):
        cube_size = 1
        cube_name = f"cube_{row}_{col}"
       
       # Get the digit from the corresponding line in the text file
        digit = int(lines[row][col])

        # Create a cube for the digit
        bpy.ops.mesh.primitive_cube_add(size=cube_size, enter_editmode=False, location=(col*cube_size, row*cube_size, digit*cube_size/4))

        cube_object = bpy.context.active_object
        # change name of each cube to be referenced later
        cube_object.name = cube_name

        # Scale the cube to the correct height
        cube_object.scale.z = digit / 2
        
        # link cube to collection
        # bpy.context.scene.collection.objects.unlink(cube_object) # throughs error when starting on empty project
        my_collection.objects.link(cube_object)




frame = 1
with open(day9_lowpoints, "r") as f:
    for line in f:
        # parse the coordinates
        row, col = map(int, line.strip().split(","))

        # find the corresponding cube object and make it glow
        cube_name = f"cube_{row}_{col}"
        if cube_name in bpy.data.objects:
            cube_object = bpy.data.objects[cube_name]

# create new material
            norm_material = bpy.data.materials.new(name= f"norm_material_{cube_name}") 
            glow_material = bpy.data.materials.new(name= f"glow_material_{cube_name}") 
            glow_material.use_nodes = True
            
            # create emission node and link it to the glow_material output node
            node_tree = glow_material.node_tree
            emission_node = node_tree.nodes.new(type="ShaderNodeEmission")
            output_node = node_tree.nodes.get("Material Output")
            node_tree.links.new(emission_node.outputs[0], output_node.inputs[0])
            
            # set strength to 2.0 to make it glow
            emission_node.inputs[1].default_value = 2.0
           
            # Assign the material and add a keyframe
            cube_object.active_material = norm_material
            cube_object.active_material.keyframe_insert(data_path="diffuse_color", index=-1, frame=frame)
            cube_object.active_material = glow_material
            cube_object.active_material.keyframe_insert(data_path="diffuse_color", index=-1, frame=frame + 25)

             # Increment the frame variable
            frame += 25
