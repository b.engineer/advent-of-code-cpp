#ifndef __AOC_DAY_16
#define __AOC_DAY_16

#include "aoc_day.h"

class AocDay16 : public AocDay {
private:
  vector<long> read_input(string filename);

public:
  AocDay16();
  ~AocDay16();
  string part1(string filename, vector<string> extra_args);
  string part2(string filename, vector<string> extra_args);
};

#endif

