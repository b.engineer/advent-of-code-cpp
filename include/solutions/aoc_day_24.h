#ifndef __AOC_DAY_24
#define __AOC_DAY_24

#include "aoc_day.h"

class AocDay24 : public AocDay {
private:
  vector<long> read_input(string filename);

public:
  AocDay24();
  ~AocDay24();
  string part1(string filename, vector<string> extra_args);
  string part2(string filename, vector<string> extra_args);
};

#endif

