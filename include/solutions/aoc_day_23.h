#ifndef __AOC_DAY_23
#define __AOC_DAY_23

#include "aoc_day.h"

class AocDay23 : public AocDay {
private:
  vector<long> read_input(string filename);

public:
  AocDay23();
  ~AocDay23();
  string part1(string filename, vector<string> extra_args);
  string part2(string filename, vector<string> extra_args);
};

#endif

