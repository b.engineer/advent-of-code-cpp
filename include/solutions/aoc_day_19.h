#ifndef __AOC_DAY_19
#define __AOC_DAY_19

#include "aoc_day.h"

class AocDay19 : public AocDay {
private:
  vector<long> read_input(string filename);

public:
  AocDay19();
  ~AocDay19();
  string part1(string filename, vector<string> extra_args);
  string part2(string filename, vector<string> extra_args);
};

#endif

