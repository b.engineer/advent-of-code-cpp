# Advent of Code 2021

Advent of Code is a programming puzzle challenge which sets two puzzles a day up until the 25th of december. I chose to use AoC to get more familier with C++ because a lot of the setup and overhead are already in place, which makes it easier start problem solving.


[Advent Of Code 2021](https://adventofcode.com/2021/)

[Project Template](https://github.com/bcooperstl/advent-of-code-cpp-starter)

[Resources Related to AoC](https://github.com/Bogdanp/awesome-advent-of-code)

[`Solutions Folder`](src/solutions)

## Building the project

After checking out the project, it's just 2 commands to build

./mkdirs.sh
make

mkdirs.sh will create the bin and build directories and their subdirectories. It only needs to be run the first time.

## Running a solution
There are three modes to run this program as shown in the usage. This section describes how to run one input file through. The [testing.md](testing.md) file describes the two other modes.
The command line is:

    bin/aoc -d day -p part -f filename [extra_args...]

Everything should be straight forward - give it the day, part (1 or 2), input file, and optionally any extra arguments. It'll spit out the result or tell you if there's an error.

For example:

    [brian@dev1 advent-of-code-cpp-starter]$ bin/aoc -d 0 -p 1 -f data/sample/day0_input.txt
    ***Day 0 Part 1 for file data/sample/day0_input.txt has result 569
